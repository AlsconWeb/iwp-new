const path = require('path');
const autoprefixer = require('autoprefixer');
const MiniCSSExtract = require('mini-css-extract-plugin');
const cssMinimizerWebpackPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = (env, argv) => {
	function isDevelopment() {
		return argv.mode === 'development';
	}

	const config = {
		entry: {
			editor: './src/js/editor.js',
			frontend: './src/js/frontend.js',
		},
		output: {
			path: path.join(__dirname, 'assets', 'js', 'core'),
			filename: '[name].js',
			clean: true,
		},
		optimization: {
			minimize: true,
			minimizer: [
				new TerserPlugin({ terserOptions: { sourceMap: true } }),
				new cssMinimizerWebpackPlugin({
					minimizerOptions: {
						map: {
							inline: false,
							annotation: true,
						},
					},
				}),
			],
		},
		plugins: [
			new CleanWebpackPlugin(),
			new MiniCSSExtract({
				chunkFilename: '[id].css',
				filename: (chunkData) => {
					return chunkData.chunk.name === 'frontend'
						? '../../css/style.css'
						: '../../css/[name].css';
				},
			}),
		],
		devtool: isDevelopment() ? 'source-map' : 'cheap-module-source-map',
		module: {
			rules: [
				{
					test: /\.m?js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							plugins: [
								'@babel/plugin-proposal-class-properties',
							],
							presets: [
								'@babel/preset-env',
								[
									'@babel/preset-react',
									{
										pragma: 'wp.element.createElement',
										pragmaFrag: 'wp.element.Fragment',
										development: isDevelopment(),
									},
								],
							],
						},
					},
				},
				{
					test: /\.(sa|sc|c)ss$/,
					use: [
						{
							loader: MiniCSSExtract.loader,
							options: {
								publicPath: path.join(
									__dirname,
									'assets',
									'css'
								),
							},
						},
						{ loader: 'css-loader' },
						{
							loader: 'postcss-loader',
							options: {
								postcssOptions: {
									plugins: [autoprefixer()],
								},
							},
						},
						{ loader: 'sass-loader' },
					],
				},
			],
		},
		externals: {
			jquery: 'jQuery',
			'@wordpress/blocks': ['wp', 'blocks'],
			'@wordpress/i18n': ['wp', 'i18n'],
			'@wordpress/editor': ['wp', 'editor'],
			'@wordpress/components': ['wp', 'components'],
			'@wordpress/element': ['wp', 'element'],
			'@wordpress/blob': ['wp', 'blob'],
			'@wordpress/data': ['wp', 'data'],
			'@wordpress/block-editor': ['wp', 'block-editor'],
		},
	};
	return config;
};
