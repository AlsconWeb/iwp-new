import { Component } from '@wordpress/element';
import {
	AlignmentToolbar,
	BlockControls,
	InspectorControls,
	RichText,
} from '@wordpress/editor';
import { PanelBody } from '@wordpress/components';
import { __ } from '@wordpress/i18n';

class Edit extends Component {
	onChangeContent = (content) => {
		this.props.setAttributes({ content });
	};

	onChangeAlignment = (alignment) => {
		this.props.setAttributes({ alignment });
	};

	render() {
		const { className, attributes } = this.props;
		const { content, alignment } = attributes;
		return (
			<>
				<InspectorControls>
					<PanelBody title={__('Panel', 'iwp')}></PanelBody>
				</InspectorControls>
				<BlockControls>
					<AlignmentToolbar
						value={alignment}
						onChange={this.onChangeAlignment}
					/>
				</BlockControls>
				<RichText
					tagName="p"
					className={className}
					onChange={this.onChangeContent}
					value={content}
					style={{ textAlign: alignment }}
				/>
			</>
		);
	}
}

export default Edit;
