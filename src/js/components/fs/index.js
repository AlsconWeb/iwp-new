import './editor.scss';
import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { RichText } from '@wordpress/editor';
import Edit from './edit';

registerBlockType('iwp/firstblock', {
	title: __('First Block !', 'iwp'),
	description: __('Our first block', 'iwp'),
	category: 'iwp-blocks',
	attributes: {
		content: {
			type: 'string',
			source: 'html',
			selector: 'p',
		},
	},

	edit: Edit,
	save: ({ attributes }) => {
		const { content } = attributes;
		return <RichText.Content tagName="p" value={content} />;
	},
});
