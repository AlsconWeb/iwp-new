import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { InnerBlocks, InspectorControls } from '@wordpress/editor';
import { PanelBody, RangeControl } from '@wordpress/components';

const attributes = {
	columns: {
		type: 'number',
		default: 3,
	},
};

registerBlockType('iwp/team-members', {
	title: __('Team Members', 'iwp'),
	description: __('Block showing a Team Member', 'iwp'),
	icon: 'grid-view',
	category: 'iwp-blocks',
	key: [__('team', 'iwp'), __('member', 'iwp')],
	support: {
		html: false,
	},
	attributes,
	edit({ className, attributes, setAttributes }) {
		const { columns } = attributes;

		return (
			<div className={`${className} col-${columns}`}>
				<InspectorControls>
					<PanelBody>
						<RangeControl
							label="Columns"
							value={columns}
							onChange={(columns) => setAttributes({ columns })}
							min={1}
							max={6}
						/>
					</PanelBody>
				</InspectorControls>
				<InnerBlocks
					allowedBlocks={['iwp/team-member']}
					template={[['iwp/team-member']]}
				/>
			</div>
		);
	},
	save({ className, attributes }) {
		const { columns } = attributes;

		return (
			<div className={`${className} col-${columns}`}>
				<InnerBlocks.Content />
			</div>
		);
	},
});
