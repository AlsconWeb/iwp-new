import { Component } from '@wordpress/element';
import {
	RichText,
	MediaPlaceholder,
	BlockControls,
	MediaUpload,
	MediaUploadCheck,
	InspectorControls,
} from '@wordpress/editor';
import { __ } from '@wordpress/i18n';
import { isBlobURL } from '@wordpress/blob';
import {
	Spinner,
	withNotices,
	Toolbar,
	IconButton,
	TextareaControl,
	PanelBody,
	SelectControl,
} from '@wordpress/components';
import { withSelect } from '@wordpress/data';

class TeamMemberEdit extends Component {
	componentDidMount() {
		const { attributes, setAttributes } = this.props;
		const { url, id } = attributes;

		if (url && isBlobURL(url) && !id) {
			setAttributes({
				url: '',
				alt: '',
			});
		}
	}

	onChangeTitle = (title) => {
		this.props.setAttributes({ title });
	};
	onChangeInfo = (info) => {
		this.props.setAttributes({ info });
	};

	onSelectImage = ({ url, alt, id }) => {
		this.props.setAttributes({ url, alt, id });
	};

	onSelectUrl = (url) => {
		this.props.setAttributes({ url, alt: '', id: null });
	};

	uploadError = (message) => {
		const { noticeOperations } = this.props;
		noticeOperation.createErrorNotice(message);
	};

	removeImage = () => {
		this.props.setAttributes({ url: '', alt: '', id: null });
	};

	updateAlt = (alt) => {
		this.props.setAttributes({ alt });
	};

	getImageSizes() {
		const { image, imageSizes } = this.props;
		if (!image) return [];
		const options = [];
		const sizes = image.media_details.sizes;
		console.log(sizes, options);

		for (const key in sizes) {
			const size = sizes[key];
			const imageSize = imageSizes.find((size) => size.slug === key);
			if (imageSize) {
				options.push({
					label: imageSize.name,
					value: size.source_url,
				});
			}
		}
		return options;
	}

	onUpdateImageSize = (url) => {
		this.props.setAttributes({ url });
	};
	onUpdateAlt = (alt) => {
		this.props.setAttributes({ alt });
	};

	render() {
		const { className, attributes, noticeUI } = this.props;
		const { title, info, url, alt, id } = attributes;
		console.log(id);
		return (
			<>
				<InspectorControls>
					<PanelBody title={__('Image Settings', 'mytheme-blocks')}>
						{url && !isBlobURL(url) && (
							<TextareaControl
								label={__('Alternative Text', 'mytheme-blocks')}
								value={alt}
								onChange={this.onUpdateAlt}
								help={__(
									'describes block with description key details'
								)}
							/>
						)}
						{id && (
							<SelectControl
								label={__('Image Size', 'mytheme-blocks')}
								options={this.getImageSizes()}
								onChange={this.onUpdateImageSize}
								value={url}
							/>
						)}
					</PanelBody>
				</InspectorControls>
				<BlockControls>
					{url && (
						<Toolbar>
							{id && (
								<MediaUploadCheck>
									<MediaUpload
										onSelect={this.onSelectImage}
										allowedTypes={['image']}
										value={id}
										render={({ open }) => {
											return (
												<IconButton
													label={__(
														'Select Image',
														'mytheme-blocks'
													)}
													onClick={open}
													icon="format-image"
												/>
											);
										}}
									/>
								</MediaUploadCheck>
							)}
							<IconButton
								label={__('Remove Image', 'mytheme-blocks')}
								onClick={this.onRemoveIamge}
								icon="trash"
							/>
						</Toolbar>
					)}
				</BlockControls>
				<div className={className}>
					{url ? (
						<>
							<img src={url} alt={alt} />
							{isBlobURL(url) && <Spinner />}
						</>
					) : (
						<MediaPlaceholder
							icon="format-image"
							onSelect={this.onSelectImage}
							onSelectURL={this.onSelectURL}
							onError={this.onUploadError}
							// accept= 'image/*'
							allowedTypes={['image']}
							notices={noticeUI}
						/>
					)}
					<RichText
						className="title"
						tagName="h4"
						onChange={this.onChangeTitle}
						value={title}
						placeholder={__('Member Name', 'iwp')}
						formattingControls={[]}
					/>
					<RichText
						className="info"
						tagName="p"
						onChange={this.onChangeInfo}
						value={info}
						placeholder={__('Info Member', 'iwp')}
						formattingControls={[]}
					/>
				</div>
			</>
		);
	}
}

export default withSelect((select, props) => {
	const id = props.attributes.id;
	return {
		image: id ? select('core').getMedia(id) : null,
		imageSizes: select('core/editor').getEditorSettings().imageSizes,
	};
})(withNotices(TeamMemberEdit));
