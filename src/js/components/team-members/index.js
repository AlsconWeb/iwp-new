import './style.editor.scss';
import parent from './parent';
import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { RichText } from '@wordpress/editor';
import edit from './edit';

const attributes = {
	title: {
		type: 'string',
		source: 'html',
		selector: 'h4',
	},
	info: {
		type: 'string',
		source: 'html',
		selector: 'p',
	},
	id: {
		type: 'number',
	},
	alt: {
		type: 'string',
		source: 'attribute',
		selector: 'img',
		attribute: 'alt',
		default: '',
	},
	url: {
		type: 'string',
		source: 'attribute',
		selector: 'img',
		attribute: 'src',
	},
};

registerBlockType('iwp/team-member', {
	title: __('Team Member', 'iwp'),
	description: __('Team Member Block', 'iwp'),
	icon: 'groups',
	parent: ['iwp/team-member'],
	support: {
		reusable: false,
		html: false,
	},
	category: 'iwp-blocks',
	keywords: [__('team', 'iwp'), __('member', 'iwp'), __('person', 'iwp')],
	attributes,
	save: ({ attributes }) => {
		const { title, info, url, alt, id } = attributes;
		return (
			<div>
				{url && (
					<img
						src={url}
						alt={alt}
						className={id ? `wp-image-${id}` : null}
					/>
				)}
				{title && (
					<RichText.Content
						className="title"
						tagName="h4"
						value={title}
					/>
				)}
				{info && (
					<RichText.Content
						className="info"
						tagName="p"
						value={info}
					/>
				)}
			</div>
		);
	},
	edit,
});
