<?php
/**
 * Theme Init Class.
 *
 * @package IWP
 */

namespace IWP;

/**
 * ThemeInit class file.
 */
class ThemeInit {
	/**
	 * Version.
	 */
	protected const IWP_VERSION = '1.0.0';
	/**
	 * Black Name.
	 */
	protected const BLOCK_NAME = [
		'firstblock',
		'team-member',
		'team-members',
	];

	/**
	 * Block Prefix.
	 */
	protected const BLOCK_PREFIX = 'iwp';

	/**
	 * ThemeInit  construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Theme Init.
	 */
	private function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_new_block' ] );
		add_action( 'enqueue_block_editor_assets', [ $this, 'register_new_block' ] );
		add_action( 'after_setup_theme', [ $this, 'theme_support' ] );
		add_filter( 'block_categories_all', [ $this, 'add_custom_category' ], 10, 1 );
	}

	/**
	 * Add Script and Style.
	 */
	public function add_scripts(): void {
		wp_enqueue_style( 'theme-style', IWP_URL . '/style.css', [], self::IWP_VERSION );
	}

	/**
	 * Add Theme Support.
	 */
	public function theme_support(): void {
		add_theme_support( 'align-wide' );
		add_theme_support(
			'post-formats',
			[
				'aside',
				'image',
				'video',
			]
		);

		// Add Default Color.
		add_theme_support(
			'editor-color-palette',
			[
				[
					'name'  => __( 'Black', 'iwp' ),
					'slug'  => 'black',
					'color' => '#292929',
				],
				[
					'name'  => __( 'Light Gray', 'iwp' ),
					'slug'  => 'light-gray',
					'color' => '#CCCCCC',
				],
				[
					'name'  => __( 'White', 'iwp' ),
					'slug'  => 'White',
					'color' => '#FFFFFF',
				],
			]
		);

		add_theme_support( 'responsive_embeds' );
	}

	/**
	 * Add Custom Category Block.
	 *
	 * @param array $block_categories Blocks Category.
	 *
	 * @return array
	 */
	public function add_custom_category( array $block_categories ): array {
		return array_merge(
			$block_categories,
			[
				[
					'slug'  => 'iwp-blocks',
					'title' => __( 'IWP Blocks', 'iwp' ),
				],
			]
		);
	}

	/**
	 * Register Gutenberg Blocks.
	 */
	public function register_new_block(): void {
		foreach ( self::BLOCK_NAME as $block ) {
			$block_name = self::BLOCK_PREFIX . '/' . $block;
			register_block_type(
				$block_name,
				[
					'editor_script' => 'iwp-first-block-edit-script',
					'editor_style'  => 'iwp-first-block-editor-style',
					'script'        => 'iwp-first-block-script',
					'style'         => 'iwp-first-block-style',
				]
			);
		}

		wp_register_script(
			'iwp-first-block-edit-script',
			get_stylesheet_directory_uri() . '/assets/js/core/editor.js',
			[
				'wp-blocks',
				'wp-i18n',
				'wp-element',
				'wp-editor',
				'wp-components',
				'wp-blob',
				'wp-data',
				'wp-block-editor',
			],
			self::IWP_VERSION,
			false
		);

		wp_register_script(
			'iwp-first-block-script',
			get_stylesheet_directory_uri() . '/assets/js/core/frontend.js',
			[ 'jquery' ],
			self::IWP_VERSION,
			false
		);

		wp_register_style(
			'iwp-first-block-editor-style',
			get_stylesheet_directory_uri() . '/assets/css/editor.css',
			[],
			self::IWP_VERSION
		);

		wp_register_style(
			'iwp-first-block-style',
			get_stylesheet_directory_uri() . '/assets/css/style.css',
			[],
			self::IWP_VERSION
		);
	}

}
