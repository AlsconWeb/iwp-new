<?php
/**
 * Created 28.11.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/\
 *
 * @package IWP
 */

use IWP\ThemeInit;

/**
 * Directory Path.
 */
const IWP_PATH = __DIR__;

/**
 * Theme Url.
 */
define( 'IWP_URL', get_stylesheet_directory_uri() );

require_once __DIR__ . '/vendor/autoload.php';

new ThemeInit();
